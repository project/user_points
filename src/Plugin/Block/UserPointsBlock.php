<?php

namespace Drupal\user_points\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "user_points_block",
 *   admin_label = @Translation("User Points Block"),
 * )
 */
class UserPointsBlock extends BlockBase {
  public function build() {
    $account = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $user = $account->get('name')->value;
    $uid = $account->get('uid')->value;

    $points = \Drupal::service('user_points.point_service')->getPoints($uid);

    return [
      '#markup' => "USER : $user <br> POINTS : $points",
    ];
  }

  public function getCacheMaxAge() {
    return 0;
  }
}
