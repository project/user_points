<?php

namespace Drupal\user_points\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting User points transactions entities.
 *
 * @ingroup user_points
 */
class UserPointsTransactionsDeleteForm extends ContentEntityDeleteForm {


}
